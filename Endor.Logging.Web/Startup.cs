﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Endor.Logging.Web.Classes;
using Serilog;
using Serilog.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.WebUtilities;
using Endor.Tenant;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Diagnostics;
using Microsoft.Extensions.Hosting;

namespace Endor.Logging.Web
{
    public class Startup
    {
        private const string ForceConsoleDebugApplicationSetting = "ForceConsoleDebug";
        private readonly bool IsDevelopment;

        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            IsDevelopment = env.IsDevelopment();
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();

        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSingleton<IEnvironmentOptions>(Configuration.GetSection("Endor").Get<LoggingAppOptions>());
            services.AddSingleton<ITenantDataCache, NetworkTenantDataCache>();
            services.AddSingleton<BusinessLogger>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
            {
                option.Audience = Configuration["Auth:ValidAudience"];
                option.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Auth:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(Configuration["Auth:SymmetricKey"])),
                };
            });
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(Configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
                //loggingBuilder.AddSerilog();
                loggingBuilder.AddDebug();

            });
            services.AddCors();
            services.AddAuthorization();
            services.AddMvc(MvcOptions =>
            {
                MvcOptions.EnableEndpointRouting = false;
            })
            .AddNewtonsoftJson(options =>
            {
                // can be removed. Added for fail-safe
                options.SerializerSettings.ReferenceLoopHandling =
                                            Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                // TODO: SET ACTUAL VALUE
                options.SerializerSettings.MaxDepth = 9999;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            if (IsDevelopment || (Configuration[ForceConsoleDebugApplicationSetting] != null && bool.TryParse(Configuration[ForceConsoleDebugApplicationSetting], out bool doForceConsoleDebug) && doForceConsoleDebug))
            {
                app.UseMiddleware<StackifyMiddleware.RequestTracerMiddleware>();
            }
            loggerFactory.AddSerilog();

            if (bool.TryParse(Configuration["SerilogDebugging"], out bool useSerilogDebugging) && useSerilogDebugging)
                Serilog.Debugging.SelfLog.Enable(msg => {
                    Controllers.DiagnosticController.LogToMemoryMetalog($"{msg}");
                    Debug.WriteLine(msg);
                });

            appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);

            app.UseAuthentication();
            app.UseCors(t => t.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseMvc();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography.X509Certificates;
using Serilog;
using Endor.Logging.Web.Classes;
using Endor.Logging.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;

namespace Endor.Logging.Web
{
    public class Program
    {
        private const string LogAllSourceContextsApplicationSetting = "LogAllSourceContexts";

        public static void Main(string[] args)
        {
            Console.Title = "Endor.Logging";
            Controllers.DiagnosticController.LogToMemoryMetalog($"Instance Started");
            IConfigurationBuilder hostingBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables();

            var host = new WebHostBuilder();
            var environment = host.GetSetting("environment");
            hostingBuilder.SetBasePath(Directory.GetCurrentDirectory());
            hostingBuilder.AddJsonFile("appsettings.json");
            hostingBuilder.AddJsonFile($"appsettings.{environment}.json", optional: true);

            if (environment == "Development")
            {
                hostingBuilder.AddUserSecrets<Startup>();
            }

            var envConfiguration = hostingBuilder.Build();
            string metalogConnString = envConfiguration["Metalog"];
            if (!String.IsNullOrWhiteSpace(metalogConnString))
            {
                bool logAllSourceContexts = GetLogAllSourceContextsOptionFromAppSettings(envConfiguration);
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .Filter.ByExcluding((log) =>
                    {
                        if (logAllSourceContexts)
                            return false;
                        else
                            return !log.Properties.ContainsKey("SourceContext") || !log.Properties["SourceContext"].ToString().StartsWith("\"Endor");
                    })
                    .WriteTo.MSSqlServer(envConfiguration["Metalog"], LoggerType.Meta.TableName()).CreateLogger();
                Controllers.DiagnosticController.LogToMemoryMetalog($"SQL Metalog configured");
            }
            else
            {
                Controllers.DiagnosticController.LogToMemoryMetalog($"Metalog connection string not found! Unable to log to Metalog");
            }

            Log.Logger.ForContext<Program>().Information("Instance started");

            BusinessLogger.FallbackConnectionString = envConfiguration.GetConnectionString("Metalog");

            Serilog.Debugging.SelfLog.Enable(msg =>
            {
                Controllers.DiagnosticController.LogToMemoryMetalog($"{msg}");
                System.Diagnostics.Debug.WriteLine(msg);
            });

            host.UseKestrel(options =>
            {
                if (environment == "Development")
                {
                    AddHttpsWithCert(options, envConfiguration);
                }
            })
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                //.UseApplicationInsights()
                .Build()
                .Run();
        }

        private static bool GetLogAllSourceContextsOptionFromAppSettings(IConfigurationRoot envConfiguration)
        {
            bool logAllSourceContexts = false;
            Controllers.DiagnosticController.LogToMemoryMetalog($"App Setting {LogAllSourceContextsApplicationSetting} is {envConfiguration[LogAllSourceContextsApplicationSetting]}");

            if (envConfiguration[LogAllSourceContextsApplicationSetting] != null && bool.TryParse(envConfiguration[LogAllSourceContextsApplicationSetting], out bool doLogAllSourceContexts) && doLogAllSourceContexts)
            {
                logAllSourceContexts = true;
            }

            return logAllSourceContexts;
        }

        private static void AddHttpsWithCert(KestrelServerOptions options, IConfigurationRoot envConfiguration)
        {
            Uri hostUri = new Uri(envConfiguration["ASPNETCORE_URLS"]);
            if(Environment.OSVersion.ToString().Contains("Unix"))
            {
                string wildCardCertPath = "/usr/local/share/ca-certificates/wildcard.localcyriousdevelopment.com.v3.GOES_IN_PERSONAL.pfx";
                string wildCardCertPassword = envConfiguration["wildCardCertPassword"];
                options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(wildCardCertPath, wildCardCertPassword)));
            }else{
                X509Store store = new X509Store("MY", StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection certs = store.Certificates.Find(X509FindType.FindByThumbprint, envConfiguration["Endor:certThumbprint"], true);
                if (certs != null && certs.Count > 0)
                {
                    X509Certificate2 cert = certs[0];
                    options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(cert)));
                }
            }
        }
    }
}

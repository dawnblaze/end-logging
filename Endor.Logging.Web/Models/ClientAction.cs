﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Logging.Models;

namespace Endor.Logging.Web.Models
{
    public class ClientAction
    {
        public short BID { get; set; }
        public ClientActionEntryType Type { get; set; }
        public int UserID { get; set; }
        public string ClientID { get; set; }
        public string Summary { get; set; }
    }
}

rm -rf publish-output;
dotnet publish -o publish-output;
docker build -t corebridge/end-logging:ci-prod .
docker push corebridge/end-logging:ci-prod
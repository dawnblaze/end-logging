﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Logging.Web.Classes
{
    public class EntryDTEnricher: ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory lepf)
        {
            logEvent.AddPropertyIfAbsent(
              lepf.CreateProperty("EntryDT", logEvent.Timestamp.UtcDateTime));
        }
    }
}

﻿using Endor.Logging.Models;
using Serilog.Sinks.MSSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Logging.Web.Classes
{
    public static class LoggerTypeExtensions
    {
        public static string TableName(this LoggerType type)
        {
            switch (type)
            {
                case LoggerType.Billing:
                    return "Log.Billing.Data";
                case LoggerType.ClientAction:
                    return "Log.Client.Action";
                case LoggerType.ClientConnection:
                    return "Log.Client.Connection";
                case LoggerType.Event:
                    return "Log.Event.Data";
                case LoggerType.Meta:
                    return "Log.Metalog";
                default:
                    return "Log.Diagnostic";
            }
        }

        public static ColumnOptions GetColumnOptions(this LoggerType type)
        {
            switch (type)
            {
                case LoggerType.ClientConnection:
                    return new ClientConnectionColumnOptions();
                case LoggerType.Billing:
                    return new BillingAndEventColumnOptions();
                case LoggerType.ClientAction:
                    return new ClientActionColumnOptions();
                case LoggerType.Event:
                    return new BillingAndEventColumnOptions();
                case LoggerType.Meta:
                    return new ColumnOptions();
                case LoggerType.Diagnostic:
                default:
                    return new DiagnosticColumnOptions();
            }
        }
    }
}

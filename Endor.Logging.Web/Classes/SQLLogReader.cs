﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Logging.Models;
using System.Data.SqlClient;

namespace Endor.Logging.Web.Classes
{
    public class SQLLogReader
    {
        private string connectionString;

        public SQLLogReader(string connstring)
        {
            this.connectionString = connstring;
        }

        public async Task<List<string[]>> GetLogs(short bid, LoggerType type, int lastNumber)
        {
            List<string[]> results = new List<string[]>();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                await conn.OpenAsync();
                SqlCommand comm = conn.CreateCommand();
                string whereClause = "";
                if (type != LoggerType.Diagnostic)
                {
                    whereClause = "WHERE BID = @BID";
                    comm.Parameters.AddWithValue("@BID", bid);
                }
                comm.CommandText = $"SELECT TOP {lastNumber} * FROM [{type.TableName()}] {whereClause} ORDER BY ID DESC";

                using (var reader = await comm.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            string[] row = new string[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                row[i] = Convert.ToString(reader.GetValue(i));
                            }
                            results.Add(row);
                        }
                    }
                }
            }

            return results;
        }
    }
}

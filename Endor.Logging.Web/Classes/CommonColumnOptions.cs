﻿using Serilog.Models;
using Serilog.Sinks.MSSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Logging.Web.Classes
{
    public class BaseColumnOptions: ColumnOptions
    {
        public BaseColumnOptions(): base()
        {
            AdditionalDataColumns = new List<DataColumn>
                {
                    new DataColumn()
                    {
                        DataType = typeof(short),
                        ColumnName = "BID"
                    },
                    new DataColumn(){
                        DataType = typeof(int),
                        ColumnName = "UserID"
                    }
                };

            Store.Remove(StandardColumn.Message);
            Store.Remove(StandardColumn.MessageTemplate);
            Store.Remove(StandardColumn.Level);
            Store.Remove(StandardColumn.TimeStamp);
            Store.Remove(StandardColumn.Exception);
            Store.Remove(StandardColumn.LogEvent);

            Properties.ExcludeAdditionalProperties = true;
        }
    }

    public class CommonColumnOptions: BaseColumnOptions
    {
        public CommonColumnOptions(): base()
        {
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(DateTime),
                ColumnName = "EntryDT"
            });
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(byte),
                ColumnName = "EntryType"
            });
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(string),
                ColumnName = "Summary"
            });
        }
    }
    public class BillingAndEventColumnOptions : CommonColumnOptions
    {
        public BillingAndEventColumnOptions() : base()
        {
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(string),
                ColumnName = "MachineName"
            });
        }
    }
    public class ClientActionColumnOptions : CommonColumnOptions
    {
        public ClientActionColumnOptions() : base()
        {
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(string),
                ColumnName = "ClientID"
            });
        }
    }
    public class ClientConnectionColumnOptions : BaseColumnOptions
    {
        public ClientConnectionColumnOptions() : base()
        {
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(string),
                ColumnName = "ClientID"
            });
            //AdditionalDataColumns.Add(new DataColumn()
            //{
            //    DataType = typeof(int),
            //    AllowDBNull = true,
            //    ColumnName = "DeviceID"
            //});
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(DateTime),
                ColumnName = "ConnectDT"
            });
            AdditionalDataColumns.Add(new DataColumn()
            {
                DataType = typeof(DateTime),
                ColumnName = "LastActivityDT"
            });
        }
    }
    public class DiagnosticColumnOptions : ColumnOptions
    {
        public DiagnosticColumnOptions(): base()
        {
            AdditionalDataColumns = new List<DataColumn>
                {
                    new DataColumn()
                    {
                        DataType = typeof(string),
                        ColumnName = "Namespace"
                    }
                };
            Store.Remove(StandardColumn.LogEvent);

            Properties.ExcludeAdditionalProperties = true;
        }

    }

}

﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Serilog;
using Endor.Logging.Models;
using Endor.Tenant;
using Microsoft.Extensions.Logging;

namespace Endor.Logging.Web.Classes
{
    public class BusinessLogger
    {
        private ConcurrentDictionary<(string connectionString, LoggerType type), Serilog.ILogger> loggers = new ConcurrentDictionary<(string connectionString, LoggerType type), Serilog.ILogger>();
        private readonly ITenantDataCache _iTenantCache;
        private Microsoft.Extensions.Logging.ILogger<BusinessLogger> _metalogger;

        public static string FallbackConnectionString { get; set; }

        public BusinessLogger(ITenantDataCache iTenantCache, Microsoft.Extensions.Logging.ILogger<BusinessLogger> metalogger)
        {
            this._iTenantCache = iTenantCache;
            this._metalogger = metalogger;
        }

        public async Task<Serilog.ILogger> Get(short bid, LoggerType type)
        {
            string logConnString = await GetLoggingDBConnectionString(bid);

            if (!loggers.TryGetValue((logConnString, type), out Serilog.ILogger result))
            {
                result = CreateNewLogger(type, logConnString);
                if (!loggers.TryAdd((logConnString, type), result))
                {
                    this._metalogger.LogWarning("Failed to add new logger");
                }
            }

            return result;
        }

        public async Task<string> GetLoggingDBConnectionString(short bid)
        {
            TenantData tenant = await _iTenantCache.Get(bid);

            if (tenant == null)
                return FallbackConnectionString;

            return tenant.LoggingDBConnectionString;
        }

        //public ConcurrentDictionary<string, Serilog.ILogger> cache = new ConcurrentDictionary<string, Serilog.ILogger>();
        //public static string GetLoggerCacheKey(short bid, LoggerType type, string connectionString)
        //{
        //    return $"{bid}|{type.ToString()}|{connectionString}";
        //}
        //public async Task<Serilog.ILogger> Get(short bid, LoggerType type)
        //{
        //    Serilog.ILogger result = null;

        //    if (this.cache.TryGetValue(GetLoggerCacheKey(bid, type, ""), out result))
        //    {
        //        return result;
        //    }
        //    else
        //    {
        //        string logDBConnectionString = await GetLoggingDBConnectionString(bid);
        //        Serilog.ILogger newLogger = CreateNewLogger(bid, type, logDBConnectionString);
        //        if (this.cache.TryAdd(GetLoggerCacheKey(bid, type, logDBConnectionString), newLogger))
        //        {
        //            return newLogger;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        private Serilog.ILogger CreateNewLogger(LoggerType type, string logDBConnectionString)
        {
            return new LoggerConfiguration()
                .Enrich.With(new EntryDTEnricher())
                .MinimumLevel.Verbose()
                .WriteTo.MSSqlServer(logDBConnectionString, type.TableName(), columnOptions: type.GetColumnOptions())
                .CreateLogger();
        }
    }
}

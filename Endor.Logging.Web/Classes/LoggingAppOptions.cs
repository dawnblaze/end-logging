﻿using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Logging.Web.Classes
{
    public class LoggingAppOptions : IEnvironmentOptions
    {
        public string AuthOrigin { get; set; }

        public string TenantSecret { get; set; }
    }
}

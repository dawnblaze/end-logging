﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.Logging.Models;
using Endor.Logging.Web.Classes;
using Serilog.Events;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Newtonsoft.Json;

namespace Endor.Logging.Web.Controllers
{
    /// <summary>
    /// Generic log controller tailored for Serilog.Sinks.HTTP (as used in Endor.Common.Logging.Client.RemoteLogger)
    /// </summary>
    [Route("api/[controller]")]
    public class LogController : Controller
    {
        private BusinessLogger _bnLogger;
        private Microsoft.Extensions.Logging.ILogger<LogController> _metalogger;

        public LogController(BusinessLogger bnLogger, Microsoft.Extensions.Logging.ILogger<LogController> metalogger)
        {
            this._bnLogger = bnLogger;
            this._metalogger = metalogger;
        }

        // GET api/log/client/1
        [HttpGet("{type}/{bid}")]
        public async Task<IActionResult> Get(string type, short bid, [FromQuery] int last = 20)
        {
            LoggerType logType;
            if (Enum.TryParse<LoggerType>(type, out logType))
            {
                var results = await new SQLLogReader(await this._bnLogger.GetLoggingDBConnectionString(bid)).GetLogs(bid, logType, last);
                return new OkObjectResult(JsonConvert.SerializeObject(results));
            }
            else
            {
                return new BadRequestResult();
            }
        }

        // POST api/log/
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] dynamic request)
        {
            if (request == null || request.events == null)
                return new BadRequestResult();

            LogEvent[] events = null;
            try
            { 
                events = JsonConvert.DeserializeObject<LogEvent[]>(request.events.ToString(), new LogEventJsonConverter(), new LogEventPropertyValueConverter());
            }
            catch (Exception e)
            {
                LogError(null, e, "Error deserializing multiple logs");

                return new BadRequestResult();
            }

            foreach(LogEvent log in events)
            {
                await WriteLog(log);
            }

            return new OkResult();
        }

        // POST api/log/1
        [HttpPost("1")]
        public async Task<IActionResult> PostSingle([FromBody]dynamic log)
        {
            if (log == null)
                return new BadRequestResult();

            LogEvent logEvent = null;
            try
            {
                logEvent = JsonConvert.DeserializeObject<LogEvent>(log.ToString(), new LogEventJsonConverter(), new LogEventPropertyValueConverter());
            }
            catch (Exception e)
            {
                LogError(logEvent, e, "Error deserializing single log");

                return new BadRequestResult();
            }

            if (logEvent == null)
                return new BadRequestResult();

            if (await WriteLog(logEvent))
                return new OkResult();
            else
                return new BadRequestResult();
        }

        private async Task<bool> WriteLog(LogEvent log)
        {
            if (log == null || log.Properties == null)
            {
                DiagnosticController.LogToMemoryMetalog($"LogController attempted to log null or property-less Log");
                this._metalogger.LogWarning("LogController attempted to log null or property-less Log");
                return false;
            }

            short BID = -1;
            LoggerType logType = LoggerType.Diagnostic;
            bool hasBID  = log.Properties.ContainsKey(Constants.BIDPropertyName) && 
                short.TryParse(log.Properties[Constants.BIDPropertyName].ToString(), out BID);

            bool hasType = log.Properties.ContainsKey(Constants.LoggerTypePropertyName) && 
                Enum.TryParse<LoggerType>(log.Properties[Constants.LoggerTypePropertyName].ToString().Trim('\"'), out logType);

            if (hasBID && hasType)
            {
                try
                {
                    (await this._bnLogger.Get(BID, logType)).Write(log);
                    DiagnosticController.LogToMemoryMetalog($"writing to log {logType.ToString()}");
                }
                catch (Exception e)
                {
                    LogError(log, e, "Error writing log");
                }

                return true;
            }
            else if (log.Properties.ContainsKey("SourceContext") && log.Properties["SourceContext"].ToString().StartsWith("\"Microsoft"))
            {
                try
                {
                    (await this._bnLogger.Get(1, LoggerType.Diagnostic)).Write(log);
                }
                catch (Exception e)
                {
                    LogError(log, e, "Error writing diagnostic log");
                }

                return true;
            }
            else
            {
                DiagnosticController.LogToMemoryMetalog($"Unable to parse log BID and type");
                this._metalogger.LogWarning("Unable to parse log BID and type. Log: {InnerLog}", log.RenderMessage());

                return false;
            }
        }

        private void LogError(LogEvent log, Exception e, string message)
        {
            DiagnosticController.LogToMemoryMetalog($"{message}: " + e.Message);
            this._metalogger.LogError(e, message+": {InnerLog}", log?.RenderMessage() ?? e.Message);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Logging.Web.Controllers
{
    [Route("api/[controller]")]
    public class DiagnosticController : Controller
    {
        
        [HttpGet("test")]
        public IActionResult Get()
        {
            return Ok("test2");
        }

        [HttpGet("memoryMetalog")]
        public IActionResult GetMemoryMetalog()
        {
            return Ok(MemoryMetalog.ToArray());
        }

        private const int MaximumInMemoryMetalogs = 75;
        private Regex iso = new Regex("(2.*Z)(.*)");
        [HttpGet("memoryMetalogDashboard")]
        public IActionResult GetMemoryMetalogDashboard()
        {
            string table = @"<table><tbody>" +
            String.Join(' ',
                MemoryMetalog.ToArray().Reverse().Select(x =>
                {
                    var match = iso.Match(x);
                    if ((match?.Groups?.Count ?? 0) == 3)
                    {
                        return $"<tr><td>{match.Groups[1].Value}</td><td>{match.Groups[2].Value}</td>";
                    }
                    else
                    {
                        return $"<tr><td colspan=2 >{match?.Groups[0]?.Value ?? x}</td>";
                    }
                }))
                + "</tbody></table>";

            return Content(@"
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>title</title>
    <style>td{border: 1px solid black; padding: 8px; } body{ padding: 10px; } table{border-collapse:collapse;} </style>
  </head>
  <body>
    <h1>Last "+ MaximumInMemoryMetalogs +@" Meta Logs</h1>
"+table+@"
  </body>
</html>
", "text/html");
        }

        [HttpGet("clearMemoryMetalog")]
        public IActionResult ClearMemoryMetalog()
        {
            MemoryMetalog.Clear();
            return Ok();
        }

        private static ConcurrentQueue<string> MemoryMetalog = new ConcurrentQueue<string>();
        public static void LogToMemoryMetalog(string message)
        {
            if (MemoryMetalog.Count > MaximumInMemoryMetalogs)
            {
                MemoryMetalog.TryDequeue(out string deadLog);
            }
            MemoryMetalog.Enqueue($"{DateTime.UtcNow.ToString("o")} {message}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.Logging.Models;
using Endor.Logging.Web.Classes;
using Serilog.Events;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Newtonsoft.Json;
using Endor.Logging.Web.Models;
using System.Data.SqlClient;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Endor.Tenant;
using Serilog;

namespace Endor.Logging.Web.Controllers
{
    /// <summary>
    /// Controller to handle POSTS direct from browser clients
    /// </summary>
    [Route("api/[controller]")]
    public class ConnectionController : Controller
    {
        private const string ClientConnectionTablename = "[Log.Client.Connection]";
        /// <summary>
        /// Cutoff for assuming what are active connections, e.g. "active connections are those that are less than 5 minutes old"
        /// </summary>
        private const int AssumedActiveThresholdMinutes = 5;
        private readonly BusinessLogger _bnLogger;
        private readonly Microsoft.Extensions.Logging.ILogger<ClientActionController> _metalogger;
        private readonly ITenantDataCache _cache;

        public ConnectionController(BusinessLogger bnLogger, Microsoft.Extensions.Logging.ILogger<ClientActionController> metalogger, ITenantDataCache _cache)
        {
            this._bnLogger = bnLogger;
            this._metalogger = metalogger;
            this._cache = _cache;
        }

        [HttpGet]
        [HttpOptions]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            if (User.BID() == null)
                return new UnauthorizedResult();

            OnlineUserIDMap result = new OnlineUserIDMap();

            await FillLoggedInUsers(result, User.BID().Value);

            return new OkObjectResult(result);
        }

        private async Task FillLoggedInUsers(OnlineUserIDMap result, short bid)
        {
            using (SqlConnection conn = new SqlConnection((await _cache.Get(bid)).LoggingDBConnectionString))
            {
                await conn.OpenAsync();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = $"SELECT DISTINCT UserID FROM {ClientConnectionTablename} WHERE BID = @BID AND [LastActivityDT] > DATEADD(minute, -{AssumedActiveThresholdMinutes},  GETUTCDATE())";
                comm.Parameters.AddWithValue("@BID", bid);

                using (var reader = await comm.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            result.Add(reader.GetInt32(0), true);
                        }
                    }
                }
            }
        }

        // POST api/connection
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ConnectionStateChangeEvent request)
        {
            if (request == null)
                return new BadRequestResult();

            await WriteStateChangeEvent(this._cache, request, async () =>
            {
                await WriteLog(request);
            });

            return new OkResult();
        }

        public static async Task WriteStateChangeEvent(ITenantDataCache cache, ConnectionStateChangeEvent request, Func<Task> onNewConnection = null)
        {
            using (SqlConnection conn = new SqlConnection((await cache.Get(request.BID)).LoggingDBConnectionString))
            {
                await conn.OpenAsync();
                if (await ConnectionExists(request, conn))
                {
                    if (request.State == ConnectionState.Reconnected)
                        await UpdateConnection(request, conn);
                    //for reporting purposes we want to leave the connection info there
                    //else if (request.State == ConnectionState.Disconnected)
                    //    await DeleteConnection(request, conn);
                }
                else
                {
                    //sometimes we just want to trigger an update
                    if (onNewConnection != null)
                        await onNewConnection();
                }
            }
        }

        private static async Task DeleteConnection(ConnectionStateChangeEvent request, SqlConnection conn)
        {
            SqlCommand comm = conn.CreateCommand();
            WriteCommandTextWithWhereClause(request, comm, $"DELETE FROM {ClientConnectionTablename}");

            await comm.ExecuteNonQueryAsync();
        }

        private static async Task UpdateConnection(ConnectionStateChangeEvent request, SqlConnection conn)
        {
            SqlCommand comm = conn.CreateCommand();
            WriteCommandTextWithWhereClause(request, comm, $"UPDATE {ClientConnectionTablename} SET LastActivityDT = @requestDT");
            comm.Parameters.AddWithValue("@requestDT", request.DateTimeUTC);

            await comm.ExecuteNonQueryAsync();
        }

        private static async Task<bool> ConnectionExists(ConnectionStateChangeEvent request, SqlConnection conn)
        {
            SqlCommand comm = conn.CreateCommand();
            try
            {

                WriteCommandTextWithWhereClause(request, comm, $"SELECT * FROM {ClientConnectionTablename}");

                using (SqlDataReader reader = await comm.ExecuteReaderAsync())
                {
                    return reader.HasRows;
                }
            }
            catch (SqlException ex)
            {
                DiagnosticController.LogToMemoryMetalog($"Error in ConnectionExists {ex.Message}");

                Log.Logger.ForContext<ConnectionController>().Error(ex, "Error in ConnectionExists");
                return false;
            }
        }

        private static void WriteCommandTextWithWhereClause(ConnectionStateChangeEvent request, SqlCommand comm, string initialClause)
        {
            comm.CommandText = initialClause + " WHERE BID = @BID and UserID = @UserID and ClientID = @ClientID";
            comm.Parameters.AddWithValue("@BID", request.BID);
            comm.Parameters.AddWithValue("@UserID", request.UserID);
            comm.Parameters.AddWithValue("@ClientID", request.ConnectionID);
        }

        private async Task<bool> WriteLog(ConnectionStateChangeEvent request)
        {
            try
            {
                (await this._bnLogger.Get(request.BID, LoggerType.ClientConnection)).ClientConnection(request.BID, request.UserID, request.ConnectionID, null, request.DateTimeUTC);
            }
            catch (Exception ex)
            {
                DiagnosticController.LogToMemoryMetalog($"Error in ConnectionController.WriteLog {ex.Message}");

                Log.Logger.ForContext<ConnectionController>().Error(ex, "Error in ConnectionController.WriteLog");
                return false;
            }

            return true;
        }
    }
}

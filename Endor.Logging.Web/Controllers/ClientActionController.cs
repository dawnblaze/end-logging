﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.Logging.Models;
using Endor.Logging.Web.Classes;
using Serilog.Events;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Newtonsoft.Json;
using Endor.Logging.Web.Models;
using Endor.Tenant;

namespace Endor.Logging.Web.Controllers
{
    /// <summary>
    /// Controller to handle POSTS direct from browser clients
    /// </summary>
    [Route("api/[controller]")]
    public class ClientActionController : Controller
    {
        private BusinessLogger _bnLogger;
        private readonly ITenantDataCache _cache;
        private Microsoft.Extensions.Logging.ILogger<ClientActionController> _metalogger;

        public ClientActionController(BusinessLogger bnLogger, Microsoft.Extensions.Logging.ILogger<ClientActionController> metalogger, ITenantDataCache cache)
        {
            this._bnLogger = bnLogger;
            this._metalogger = metalogger;
            this._cache = cache;
        }

        // POST api/clientaction
        [HttpPost]
        [HttpOptions]
        public async Task<IActionResult> Post([FromBody] ClientAction request)
        {
            if (request == null)
                return new BadRequestResult();

            await WriteLog(request);

            return new OkResult();
        }

        private async Task<bool> WriteLog(ClientAction request)
        {
            (await this._bnLogger.Get(request.BID, LoggerType.ClientAction)).ClientAction(request.Type, request.BID, request.UserID, request.ClientID, request.Summary);

            //update the lastActivityDT for this client
            await ConnectionController.WriteStateChangeEvent(this._cache, new ConnectionStateChangeEvent() {
                BID = request.BID,
                UserID = request.UserID,
                ConnectionID = request.ClientID,
                State = ConnectionState.Reconnected,
                DateTimeUTC = DateTime.UtcNow,
            });

            return true;
        }
    }
}
